
### PSBot (proposal)

#### Intro

This bot takes one space (the one to manage) and one control room.
The control room should be encrypted and invite only.

And ofcourse, the bot needs its own dedicated account.

#### Prerequisites

Create a matrix account for the bot. No need to login, the bot will love virgin account on first startup.

#### Install

clone the repos

  git clone https://a.url.no/one/told/you

You know how to use python stuff… or use the freaking docker image. See ``compose.yaml``, data are stored in ``./notdecidedyet/ ``

  docker compose build
  docker compose up -d

#### Manual Setup

create space and controlroom, invite the bot and make it space admin. then copy psbot-config.example.toml to psbot-config.toml and fill it in. run bot.

#### Assisted Setup

First let the bot create the space and control room for you (not yet)

  psbot.py init homeserverurl mxid:psbot password spacealias mxid:controlrominvite [mxid:controlrominvite... ] 

This creates the space, the control room, invites the given users to the control room, and writes the configuration.

#### Run the bot

  psbot.py

Yaey, thats is.

#### Configure (control room)

Accept an invite to the control room.

  !help

  !section list
  !section create type name [parent]
  !section remove
  !section get name option
  !section set name option value

  !config set option value
  !config get [option]
  !config print
  !config commit

##### Sections

Types:

- location
- project
- regular
- event
** future
** current
** past
- space (hack & makerspaces)

#### Register room/space

open dm with bot

  !register #roomorspacealias:example.com

to start a new registration or

  !modify #roomorspacealias:example.com

to edit an existing one.

  !print
  #optionname [value]

to apply your settings

  !commit

#### Search room/space

open DM with bot.

Any line not starting with '!' is taken as search request, so you can omitt the leading ``!search``.
    
    !search [option:value... ] [searchterm...]
    !search location:c-base topic:matrix
    !search location:c-base foo bar

``searchterm`` does a full text search on the room descriptions,
or simply browse the space hierarchy using your matrix client.

#### Search room/space (far in the future)

open DM with bot

  !search widget

enables the search widget. parametric search like electronic components.

alternatively (if configured) you can join the ``search widget room``, but output methods may be less feature rich then DM.

#### tin foil hat hints

to have the least possible data traces, do not click/join any of the index space(s).

DM the bot, search & find your rooms/spaces and join them directly.

----
