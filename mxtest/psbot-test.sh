#!/bin/sh

set -eu

# include configuration
. ./.env

# start the bot
mxcompose up -d --wait psbot || exit_code=$?
if [ -n "${exit_code:-}" ]; then
  echo "Bot did not start. Wait for some log and bye (23)."
  sleep 5
  mxcompose logs psbot
  exit 23
fi

# let it settle
sleep 10

mxcompose logs psbot
