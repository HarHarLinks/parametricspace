#!/bin/sh

set -eu

# include configuration
. ./.env

# create space & control room for psbot
mxcompose run --rm mcomm --log-level ERROR ERROR --credentials /profiles/psbot.json --store /profiles/psbot --login password --homeserver http://synapse:8008 --user-login "@psbot:synapse" --password psbot --device psbot --room-default "#alias:synapse"
psbot_token=$(jq -r '.access_token' data/profiles/psbot.json)
mxcompose run --rm debshell /bin/bash -c 'curl -sS -X POST "http://synapse:8008/_matrix/client/v3/createRoom?access_token='${psbot_token}'" -H "Accept: application/json" -H "Content-Type: application/json" -d '\''{"creation_content":{"type":"m.space"},"name":"Parametric Space","preset":"public_chat"}'\' | jq -r '.room_id' > ./data/profiles/psbot.spaceid
mxcompose run --rm mcomm --log-level ERROR ERROR --credentials /profiles/psbot.json --store /profiles/psbot --room-create ParametricControlRoom --name ParametricSpaceControlRoom --topic 'Paramatric Bot Control Room' | sed 's/\s.*$//' > ./data/profiles/psbot.roomid

# bot, dude
mkdir -p ${MXTEST_HOME}/data/psbot-home
echo "[simplematrixbotlib.config]" > ${MXTEST_HOME}/data/psbot-home/psbot-config.toml
echo "parametric_space = '$(cat ./data/profiles/psbot.spaceid)'" >> ${MXTEST_HOME}/data/psbot-home/psbot-config.toml
echo "control_room = '$(cat ./data/profiles/psbot.roomid)'" >> ${MXTEST_HOME}/data/psbot-home/psbot-config.toml
