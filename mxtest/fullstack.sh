#!/bin/sh

set -eu

# you may have mxtest installed, ignore it
if [ -n $MXTEST_SDK_ROOT ]; then
  unset MXTEST_SDK_ROOT
fi

# attempt to kill previous instance
if [ -f .env ]; then
  docker compose down --remove-orphans
fi

# first install mxtest into ./mxtest if not present
if [ ! -d mxtest ]; then
  # extract from docker image
  image=registry.gitlab.com/mb-saces/mxtest:next
  docker pull $image
  container_id=$(docker create "$image")
  docker cp "$container_id:/mxtest" mxtest
  docker rm "$container_id"
fi

# activate it by adding it to path
export PATH=$(pwd)/mxtest/bin:${PATH}
export MXTEST_SDK_ROOT=$(pwd)/mxtest

# remove leftovers from previous run
mxtest purge


# setup synapse and create users and user content
./setup.sh


./psbot-assisted.sh

# start firefox, try to login...
mxcompose up -d cinny-web
mxcompose up -d element-web
mxcompose run --rm firefox firefox http://cinny-web http://element-web
