#!/bin/sh

set -eu

# create default .env
mxtest ci-init


# include configuration
. ./.env

# first build the psbot container, if this fails we don't need to continiue
mxcompose build psbot

mxcompose build debshell

# configure services we want to use
${MXTEST_SDK_ROOT}/hs/synapse/setup.sh
cat ./synapse.ratelimit.yaml >> ${MXTEST_HOME}/data/synapse/homeserver.yaml

# start and wait for synapse to be healthy
mxcompose up -d --wait synapse

# create directory for matrix-commander
mkdir -p ${MXTEST_HOME}/data/profiles

# do something with service, create users for examples
mxcompose exec synapse register_new_matrix_user -c /data/homeserver.yaml -u serveradmin -p serveradmin -a
mxcompose exec synapse register_new_matrix_user -c /data/homeserver.yaml -u psbot -p psbot --no-admin

for i in $(seq 10)
do
  mxcompose exec synapse register_new_matrix_user -c /data/homeserver.yaml -u user${i} -p user${i} --no-admin
done

# generate token for server admin
mxcompose run --rm mcomm --log-level ERROR ERROR --credentials /profiles/serveradmin.json --store /profiles/serveradmin --login password --homeserver http://synapse:8008 --user-login "@serveradmin:synapse" --password serveradmin --device serveradmin --room-default "#alias:synapse"

# create user data
for i in $(seq 10)
do
  mxcompose run --rm mcomm --log-level ERROR ERROR --credentials /profiles/user${i}.json --store /profiles/user${i} --login password --homeserver http://synapse:8008 --user-login "@user${i}:synapse" --password user${i} --device user${i} --room-default "#alias:synapse"
  ac_token=$(jq -r '.access_token' data/profiles/user${i}.json)
  mxcompose run --rm debshell /bin/bash -c 'curl -sS -X POST "http://synapse:8008/_matrix/client/v3/createRoom?access_token='${ac_token}'" -H "Accept: application/json" -H "Content-Type: application/json" -d '\''{"creation_content":{"type":"m.space"},"name":"User'${i}' Curl Space","preset":"public_chat"}'\' | jq -r '.room_id' > ./data/profiles/user${i}.spaceid
done
