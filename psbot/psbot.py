import logging
import re
from enum import StrEnum
from typing import Dict, Tuple, List
import simplematrixbotlib as botlib
from dataclasses import dataclass
from nio import RoomVisibility, RoomGetStateEventError, RoomCreateResponse, RoomPutStateError, \
    RoomGetStateEventResponse, RoomGetStateResponse, RoomResolveAliasResponse, RoomPutStateResponse
from datetime import datetime, timezone
from simplematrixbotlib.api import split_mxid


@dataclass
class PSBotConfig(botlib.Config):
    _parametric_space: str = "ParametricSpace handle not set"
    _control_room: str = "Control room handle not set"
    _homeserver: str = "Homeserver not set"
    _user: str = "User not set"
    _password: str = "Password not set"

    @property
    def parametric_space(self) -> str:
        return self._parametric_space

    @parametric_space.setter
    def parametric_space(self, value: str) -> None:
        self._parametric_space = value

    @property
    def control_room(self) -> str:
        return self._control_room

    @control_room.setter
    def control_room(self, value: str) -> None:
        self._control_room = value

    @property
    def homeserver(self) -> str:
        return self._homeserver

    @homeserver.setter
    def homeserver(self, value: str) -> None:
        self._homeserver = value

    @property
    def user(self) -> str:
        return self._user

    @user.setter
    def user(self, value: str) -> None:
        self._user = value

    @property
    def password(self) -> str:
        return self._password

    @password.setter
    def password(self, value: str) -> None:
        self._password = value


class SUBSPACE(StrEnum):
    PAST = "past"
    CURRENT = "current"
    UPCOMING = "upcoming"
#    RECURRING = "recurring"  # todo regular?


config = PSBotConfig()
config.load_toml('psbot-config.toml')
creds = botlib.Creds(config.homeserver, config.user, config.password)
bot = botlib.Bot(creds, config)
PREFIX = '!'

ROOM_STATE_SPACE = 'de.matrixmeetup.space'
ROOM_STATE_EVENT = 'de.matrixmeetup.event'

RE_SEVER_NAME = r'(?P<hostname>(?P<ipv4>\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3})|(?P<ipv6>\[[\da-fA-F:.]{2,45}\])|(?P<dns>[a-zA-Z\d\-.]{1,255}))(?P<port>:\d{1,5})?'
RE_ROOM_ID = re.compile(r'!.*:' + RE_SEVER_NAME)
RE_ROOM_ALIAS = re.compile(r'#.*:' + RE_SEVER_NAME)


@bot.listener.on_startup
async def room_joined(room_id):
    print(f"This account is a member of a room with the id {room_id}")


@bot.listener.on_message_event
async def help_message(room, message):
    match = botlib.MessageMatch(room, message, bot, PREFIX)
    if not (match.is_not_from_this_bot() and match.prefix()
            and match.command("help")):
        return

    message = (f"""
parametric space bot
- `{PREFIX}help`: show this help
- `{PREFIX}echo [text...]`: repeats the text back to you
- `{PREFIX}register <room ID|room alias> <start: YYYY-MM-DD[[ HH:MM]+HH[:MM]]> <end: YYYY-MM-DD[[ HH:MM]+HH[:MM]]>`: adds the room as a child to the parametric space

{_linkify("parametric space", config.parametric_space)}
{_linkify("control room", config.control_room)}
""")
    await bot.api.send_markdown_message(room.room_id, message)


@bot.listener.on_message_event
async def echo(room, message):
    match = botlib.MessageMatch(room, message, bot, PREFIX)

    if match.is_not_from_this_bot() and match.prefix() and match.command("echo"):
        await bot.api.send_text_message(
            room.room_id, " ".join(arg for arg in match.args())
        )


@bot.listener.on_message_event
async def register(room, message):
    match = botlib.MessageMatch(room, message, bot, PREFIX)
    if match.is_not_from_this_bot() and match.prefix() and match.command("register"):

        subspace_room_ids = await _setup()

        if not match.args() or len(match.args()) != 3:
            await _send_register_help(room)
            return

        event_room_id = match.args()[0]

        if RE_ROOM_ALIAS.search(event_room_id):
            result = await bot.async_client.room_resolve_alias(event_room_id)
            if isinstance(result, RoomResolveAliasResponse):
                event_room_id = result.room_id

        if not RE_ROOM_ID.search(event_room_id):
            await _send_register_help(room)
            return

        new = False
        event = await bot.async_client.room_get_state_event(room_id=config.parametric_space,
                                                            event_type=ROOM_STATE_EVENT,
                                                            state_key=event_room_id)
        if isinstance(event, RoomGetStateEventError):
            event_details = dict()
            new = True
        else:
            event_details = event.content

        start = int(datetime.fromisoformat(match.args()[1]).timestamp())
        end = int(datetime.fromisoformat(match.args()[2]).timestamp())

        event_details['start_ts'] = start
        event_details['end_ts'] = end

        new_subspace, prev_subspaces = await _move_event_in_subspaces(event_room_id,
                                                                      event_details,
                                                                      subspace_room_ids)

        # save event info to parametric space
        result = await bot.async_client.room_put_state(room_id=config.parametric_space,
                                                       event_type=ROOM_STATE_EVENT,
                                                       content=event_details,
                                                       state_key=event_room_id)
        if isinstance(result, RoomPutStateError):
            raise Exception("could not save event details")
        logging.info(f"update event info in parametric space {config.parametric_space}: {result}")

        await bot.api.send_markdown_message(
            room.room_id,
            f"{'registered' if new else 'updated'} {_format_list(prev_subspaces, subspace_room_ids)} -> " +
            f"{_linkify_subspace(new_subspace, subspace_room_ids)} event {_linkify(event_room_id, event_room_id)}:\n" +
            f"- start {datetime.fromtimestamp(start, tz=timezone.utc)} UTC (`{start}`)\n" +
            f"- end {datetime.fromtimestamp(end, tz=timezone.utc)} UTC (`{end}`)")


async def _send_register_help(room):
    await bot.api.send_markdown_message(
        room.room_id, "Help: `!register <room ID|room alias> <start> <end>`"
    )


def _format_list(prev_subspaces, subspace_room_ids):
    prev_subspaces = [_linkify_subspace(prev, subspace_room_ids) for prev in prev_subspaces]
    return str(prev_subspaces) if len(prev_subspaces) != 1 else prev_subspaces.pop()


async def _move_event_in_subspaces(event_room_id: str,
                                   event_details: Dict[str, int],
                                   subspace_room_ids: Dict[str, str]) -> Tuple[SUBSPACE, List[SUBSPACE]]:
    """
    Parameters
    ----------
    event_room_id: room ID of the room (or space) that represents an event, conference, meeting, etc.
    event_details: event start/end info {'start_ts': timestamp, 'end_ts': timestamp}
    subspace_room_ids: dict that maps the names of the parametric space subspaces to their room IDs

    Returns
    -------
    Tuple of names of the new subspace and list of the previous subspaces' names.
    Previous can be None to indicate the event has not been moved.
    """

    start = event_details['start_ts']
    end = event_details['end_ts']
    now = int(datetime.now().timestamp())

    subspace = "unknown"
    if now < start:
        subspace = SUBSPACE.UPCOMING
    elif start <= now < end:
        subspace = SUBSPACE.CURRENT
    elif end <= now:
        subspace = SUBSPACE.PAST
    subspace_room = subspace_room_ids[subspace]

    # put room into the correct subspace
    result = await bot.async_client.room_put_state(room_id=subspace_room,
                                                   event_type='m.space.child',
                                                   content={'via': [_homeserver()]},
                                                   state_key=event_room_id)
    if isinstance(result, RoomPutStateError):
        raise Exception(f"could not put {event_room_id} into {subspace_room}")
    logging.info(f"put room into subspace {subspace}: {result}")

    # remove from incorrect subspaces
    prev_subspaces = list()
    for s in SUBSPACE:
        if s != subspace:
            wrong_subspace = subspace_room_ids[s]
            is_child = await bot.async_client.room_get_state_event(room_id=wrong_subspace,
                                                                   event_type='m.space.child',
                                                                   state_key=event_room_id)
            # don't spam the room state every time
            if isinstance(is_child, RoomGetStateEventResponse) and is_child.content != {}:
                result = await bot.async_client.room_put_state(room_id=wrong_subspace,
                                                               event_type='m.space.child',
                                                               content={},
                                                               state_key=event_room_id)
                if isinstance(result, RoomPutStateError):
                    raise Exception(f"could not remove {event_room_id} from {wrong_subspace}")
                prev_subspaces.append(s)
                logging.info(f"remove room from wrong subspace {subspace_room}: {result}")
    return subspace, prev_subspaces


@bot.listener.on_message_event
async def debug(room, message):
    match = botlib.MessageMatch(room, message, bot, PREFIX)

    if match.is_not_from_this_bot() and match.prefix() and match.command("debug"):
        await _setup()


async def _create_space(name: str):
    result = await bot.async_client.room_create(name=name,
                                                room_type='m.space',
                                                visibility=RoomVisibility.public)
    if not isinstance(result, RoomCreateResponse):
        raise Exception(f"could not create space {name}")
    return result


async def _set_subspace(parent: str, child: str):
    """
    Parameters
    ----------
    parent: space room ID
    child: room ID
    """
    result = await bot.async_client.room_put_state(room_id=parent,
                                                   event_type='m.space.child',
                                                   content={'via': [_homeserver()], 'suggested': True},
                                                   state_key=child)
    if not isinstance(result, RoomPutStateResponse):
        raise Exception(f"could not set subspace {parent}")


async def _setup():
    subspaces = await bot.async_client.room_get_state_event(config.parametric_space, ROOM_STATE_SPACE)
    if isinstance(subspaces, RoomGetStateEventError):
        subspace_room_ids = dict()
    else:
        subspace_room_ids = subspaces.content

    for subspace in SUBSPACE:
        room_id = subspace_room_ids.get(subspace)
        if room_id is None:
            room_id_response = await _create_space(subspace)
            room_id = room_id_response.room_id
            await _set_subspace(config.parametric_space, room_id)
        subspace_room_ids[subspace] = room_id
    result = await bot.async_client.room_put_state(room_id=config.parametric_space,
                                                   event_type=ROOM_STATE_SPACE,
                                                   content=subspace_room_ids)
    if isinstance(result, RoomPutStateError):
        raise Exception(f"could not set up subspaces")
    logging.info(f"subspaces: {subspace_room_ids}")
    return subspace_room_ids


def _homeserver():
    return split_mxid(creds.username)[1]


def _linkify(label, mxid):
    return f"[`{label}`](https://matrix.to/#/{mxid})"


def _linkify_subspace(subspace, room_ids):
    return _linkify(subspace, room_ids.get(subspace))


@bot.listener.on_message_event
async def update(room, message):
    match = botlib.MessageMatch(room, message, bot, PREFIX)
    if match.is_not_from_this_bot() and match.prefix() and match.command("update"):

        result = await bot.async_client.room_get_state(config.parametric_space)
        if isinstance(result, RoomGetStateResponse):
            subspace_room_ids = await _setup()

            state = result.events
            moves = list()
            for s in state:
                if s.get('type', '') == ROOM_STATE_EVENT:
                    event_room_id = s.get('state_key')
                    event_details = s.get('content')
                    logging.debug(f"updating {event_room_id}")
                    subspace, prev_subspaces = await _move_event_in_subspaces(event_room_id,
                                                                              event_details,
                                                                              subspace_room_ids)
                    if len(prev_subspaces) > 0:
                        moves.append(f"* {_linkify(event_room_id, event_room_id)}: " +
                                     f"{_format_list(prev_subspaces, subspace_room_ids)}"
                                     f"-> {_linkify_subspace(subspace, subspace_room_ids)}")

            list_str = '\n'.join(moves)
            if len(moves) > 0:
                msg = f"moved\n{list_str}"
            else:
                msg = "nothing to update"
            await bot.api.send_markdown_message(room.room_id, msg)


# todo regularly check for rooms in the space hierarchy that are incorrectly registered and notify the control roo


logging.info(f"parametric space id {config.parametric_space}")
logging.info(f"control room id {config.control_room}")

bot.run()
