#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# this file lives at its birthplace for now (psbot), but will move to https://gitlab.com/determinant/mxpassfile-py
# or so after reaching some maturity.

"""mxpassfile
   simple python implementation of https://gitlab.com/determinant/mxpassfile without tag support

"""
import os
from os import path
import platform
import stat

def gettoken(matrixhost="*", localpart="*", domain="*"):
    """Return the password for the specified matrixhost, localpart and domain.
    :py:const:`None` will be returned if a token can not be found for the
    specified connection parameters.

    If the mxpass file can not be located, a :py:class:`FileNotFound`
    exception will be raised.

    If the mxpass file is group or world readable, the file will not be read,
    per the specification, and a :py:class:`InvalidPermissions` exception will
    be raised.

    If an entry in the password file is not parsable, a
    :py:class:`InvalidPermissions` exception will be raised.

    :param str matrixhost: homeserver hostname
    :param str localpart: localpart of matrix id
    :param str domain: domain of matrix id
    :rtype: str
    :raises: FileNotFound
    :raises: InvalidPermissions
    :raises: InvalidEntry

    """
    for entry in _get_entries():
        if entry.match(matrixhost, localpart, domain):
            return entry.token
    return None


class MxPassException(Exception):
    """Base exception for all mxpassfile exceptions"""
    MESSAGE = 'Base Exception: {}'

    def __str__(self):
        return self.MESSAGE.format(*self.args)


class FileNotFound(MxPassException):
    """Raised when the password file specified in the PGPASSFILE environment
    variable or ``.pgpass`` file in the user's home directory does not exist.

    """
    MESSAGE = 'No such file "{0}"'


class InvalidEntry(MxPassException):
    """Raised when the mxpass file can not be parsed properly due to errors
    in the entry format.

    """
    MESSAGE = 'Error validating {0} value "{1}"'


class InvalidPermissions(MxPassException):
    """Raised when the mxpass file specified in the MXPASSFILE environment
    variable or ``.mxpass`` file in the user's home directory has group or
    world readable permission bits set.

    """
    MESSAGE = 'Invalid Permissions for {0}: {1}'

class _Entry(object):
    """Encapsulate a single entry from the mxpass file and provide a method
    for checking to see if the entry matches the matrixhost, localpart, and domain
    values.

    :param str matrixhost: The homeserver url
    :param str localpart: The localpart of mxid
    :param str domain: The domain of mxid
    :param str token: The password/token

    """

    def __init__(self, matrixhost, localpart, domain, token):
        self.matrixhost = matrixhost
        self.localpart = localpart
        self.domain = domain
        self.token = token

    def match(self, matrixhost, localpart, domain):
        """Evaluate the matrixhost, localpart, domain combination against the
        entry values.

        :param str matrixhost: The matrix homeserver url
        :param int localpart: The localpart of mxid
        :param str domain: The domain of mxid
        :rtype: bool

        """
        return all([any([self.matrixhost == '*', self.matrixhost == matrixhost]),
                    any([self.localpart == '*', self.localpart == localpart]),
                    any([self.domain == '*', self.domain == domain])])


def _file_path():
    """Return the path to the mxpass file, checking first for the value of
    the MXPASSFILE environment variable, falling back to ``.mxpass`` in the
    user's home directory.

    On Microsoft Windows, it is assumed that the file is stored in a directory
    that is secure, so no special permissions check is made.

    :return: str
    :raises: FileNotFound
    :raises: InvalidPermissions

    """
    file_path = os.environ.get('MXPASSFILE', _default_path())
    if not path.exists(file_path):
        raise FileNotFound(file_path)

    if platform.system() != 'Windows':
        file_stat = os.stat(file_path)
        if ((file_stat.st_mode & stat.S_IRGRP == stat.S_IRGRP) or
                (file_stat.st_mode & stat.S_IROTH == stat.S_IROTH)):
            raise InvalidPermissions(file_path,
                                     oct(stat.S_IMODE(file_stat.st_mode)))
    return file_path

def _default_path():
    """Return the default path of .mxpass in the current user's home directory

    :rtype: str

    """
    if platform.system() == 'Windows':
        return path.join(os.getenv('APPDATA', path.join(path.expanduser('~'), 'AppData')),
                                   'matrix', 'mxpass.conf')
    return path.join(path.expanduser('~'), '.mxpass')


#with open(filename, 'r', encoding='UTF-8') as file:
#    while line := file.readline():
#        print(line.rstrip())

def _get_entries():
    """Return a list of the entries in the pgpass file as a list of _Entry
    instances.

    :return: list

    """
    entries = list()

    with open(_file_path(), mode='r', encoding='UTF-8') as mxpass_file:
        while mxline := mxpass_file.readline().rstrip():
            if mxline.startswith("#"):
                continue
            if len(mxline) == 0:
                continue
            entries.append(_parse_line(mxline))
    return entries


_TMPBACKSLASH = "\r"
_TMPPIPE      = "\n"

def _unescape(mxline):
    mxline = mxline.replace(_TMPBACKSLASH, "\\")
    mxline = mxline.replace(_TMPPIPE, "|")
    return mxline

def _parse_line(mxline):
        oline = mxline
        mxline = mxline.replace("\\\\", _TMPBACKSLASH)
        mxline = mxline.replace("\\|", _TMPPIPE)

        parts = mxline.split("|")
        if len(parts) < 4:
            raise InvalidEntry("too short mxpass line", oline)

        return _Entry(_unescape(parts[0]), _unescape(parts[1]), _unescape(parts[2]), _unescape(parts[3]))

def mxpass_cli():
    import argparse
    import sys
    parser = argparse.ArgumentParser()
    parser.add_argument('-s', '--homeserver', default="*", help="homeserver url")
    parser.add_argument('-u', '--localpart', default="*", help="localpart")
    parser.add_argument('-d', '--domain', default="*", help="domain")
    # TODO add tag support
    # parser.add_argument('-t', '--tag', action='append', dest='tags', help="tag")
    if len(sys.argv)==1:
        parser.print_help(sys.stderr)
        sys.exit(1)
    args = parser.parse_args()
    
    #result = args.number * 2
    print("Result:", gettoken(args))

if __name__ == "__main__":
    mxpass_cli()
